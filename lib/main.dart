import 'package:flutter/material.dart';
import 'package:web2/channel_setts.dart';

import 'main_wgt.dart';
import 'db/db_controller.dart';

void main() => runApp(MyApp());


MainWgt _mainWgt;


class MyApp extends StatelessWidget {
  @override

  Widget build(BuildContext context) {
    _mainWgt = new MainWgt();

    return MaterialApp(
      initialRoute: '/',
      routes: {
        // When navigating to the "/" route, build the FirstScreen widget.
        '/main': (context) => _mainWgt,
        // When navigating to the "/second" route, build the SecondScreen widget.
        '/channel_setts': (context) => _mainWgt.getChanSettsWgt(),
        '/controller_setts': (context) => _mainWgt.getCntrlSettsWgt(),
      },
      home: Scaffold(
        backgroundColor: Colors.grey[200],
        body: SignUpScreen(),      
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}

class SignUpScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        width: 400,
        child: Card(
          child: SignUpForm(),
        ),
      ),
    );
  }
}

class SignUpForm extends StatefulWidget {
  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm>
    with SingleTickerProviderStateMixin {
  bool _formCompleted = false;
  AnimationController animationController;
  Animation<Color> colorAnimation;

  @override
  void initState() {
    super.initState();

    animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 1200));

    var colorTween = TweenSequence([
      TweenSequenceItem(
        tween: ColorTween(begin: Colors.red, end: Colors.orange),
        weight: 1,
      ),
      TweenSequenceItem(
        tween: ColorTween(begin: Colors.orange, end: Colors.yellow),
        weight: 1,
      ),
      TweenSequenceItem(
        tween: ColorTween(begin: Colors.yellow, end: Colors.green),
        weight: 1,
      ),
    ]);

    colorAnimation = animationController.drive(colorTween);

  }

  void _showWelcomeScreen() {
    Navigator.pushNamed(context, '/main');
    // Navigator.of(context)
    //     .push(MaterialPageRoute(builder: (context) => WelcomeScreen()));
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        AnimatedBuilder(
          animation: animationController,
          builder: (context, child) {
            return LinearProgressIndicator(
              value: animationController.value,
              valueColor: colorAnimation,
              backgroundColor: colorAnimation.value.withOpacity(0.4),
            );
          },
        ),

        SignUpFormBody(
          onProgressChanged: (progress) {
            if (!animationController.isAnimating) {
              animationController.animateTo(progress);
            }
            setState(() {
              _formCompleted = progress == 1;
            });
          },
        ),
        Container(
          height: 40,
          width: double.infinity,
          margin: EdgeInsets.all(12),
          child: FlatButton(
            color: Colors.blue,
            textColor: Colors.white,
            onPressed: (){ 
              SQLiteDbProvider.db.database;
              _showWelcomeScreen();

               _mainWgt.addChanSettings(ChannelSetts(
                SettsRole.editSetts,
                ID                : 17,
                IsWorking         : true,
                InpNameLat        : "TZZ",
                OutNameLat        : "ZTZ",
                CcmAddressLat     : "CCMMCCMM",
                OwnAddressLat     : "URKGABCD",
                CcmIpAddress      : "192.168.1.24",
                CcmIpPort         : 81,
                FdpsWsPort        : 9090,
                InpNum            : 233,
                OutNum            : 443,
                OutNumSkipCounter : 2,
                CrLf              : "\\r\\n",
                SplitSignal       : true,
                ChControl         : false,
                WorkTime          : "круглосуточно",
                SSConfirm         : false,
                QCatSvcLat        : "SS",
                MaxOutTlgLen      : 8196,
                MaxInpTlgLen      : 8196,
                ChFormat          : "ЦХ ЛР",
                PrevDateLs        : 231,
                PrevDateLr        : 123,
                Version           : "2020-03-03",
                StatInpBytesCount : 4096, 
                StatOutBytesCount : 4096,
                URLAddress        : "192.168.1.219",
                URLPath           : "/chan",
                URLPort           : 8085,
                ShowDebugMgs      : false,
              ));
            },
            child: Text('Sign up'),
          ),
        ),
      ],
    );
  }
}

class SignUpFormBody extends StatefulWidget {
  final ValueChanged<double> onProgressChanged;

  SignUpFormBody({
    @required this.onProgressChanged,
  }) : assert(onProgressChanged != null);

  @override
  _SignUpFormBodyState createState() => _SignUpFormBodyState();
}

class _SignUpFormBodyState extends State<SignUpFormBody> {
  static const EdgeInsets padding = EdgeInsets.all(8);
  final TextEditingController emailController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController websiteController = TextEditingController();

  List<TextEditingController> get controllers =>
      [emailController, phoneController, websiteController];

  @override
  void initState() {
    super.initState();
    controllers.forEach((c) => c.addListener(() => _updateProgress()));
  }

  double get _formProgress {
    var progress = 0.0;
    for (var controller in controllers) {
      if (controller.value.text.isNotEmpty) {
        progress += 1 / controllers.length;
      }
    }
    return progress;
  }

  void _updateProgress() {
    widget.onProgressChanged(_formProgress);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: padding,
            child: Text('Sign up', style: Theme.of(context).textTheme.display1),
          ),
          SignUpField(
            hintText: 'E-mail address',
            controller: emailController,
          ),
          SignUpField(
            hintText: 'Phone number',
            controller: phoneController,
          ),
          SignUpField(
            hintText: 'Website',
            controller: websiteController,
          ),
        ],
      ),
    );
  }
}

class SignUpField extends StatelessWidget {
  final String hintText;
  final TextEditingController controller;

  SignUpField({
    @required this.hintText,
    @required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(8),
      child: TextFormField(
        decoration: InputDecoration(hintText: hintText),
        controller: controller,
      ),
    );
  }
}

class WelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold( body: _mainWgt);
  }
}




