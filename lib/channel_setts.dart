
enum SettsRole { 
   newSetts, 
   editSetts,
   none
}

class ChannelSetts {
  SettsRole role        ;
  int ID                ;// `json:"ChannelID"`          идентификатор канала
	bool IsWorking        ;// `json:"IsWorking"`          работоспособность канала
	String InpNameLat     ;// `json:"InpNameLat"`         обозначение канала на прием лат
	String OutNameLat     ;// `json:"OutNameLat"`         обозначение канала на передачу лат
	String CcmAddressLat  ;// `json:"CcmAddressLat"`      адрес цкс лат
	String OwnAddressLat  ;// `json:"OwnAddressLat"`      главный адресный указатель лат (свой адрес)
 	String CcmIpAddress   ;// `json:"CcmIpAddress"`       IP адрес ЦКС
	int CcmIpPort         ;// `json:"CcmIpPort"`          порт подключения к ЦКС
	int FdpsWsPort        ;// `json:"FdpsWsPort"`         порт подключения fdps-aftn
	int InpNum            ;// `json:"InpNum"`             последний полученный канальный номер
	int OutNum            ;// `json:"OutNum"`             последний отправленный канальный номер
	int OutNumSkipCounter ;// `json:"OutNumSkipCounter"`  количество игнорируемых сообщений о подстройке канальных номеров
	String CrLf           ;// `json:"CrLf"`               символы конца строки  (\r\n, \r\r\n)
	bool SplitSignal      ;// `json:"SplitSignal"`        сигнал разделения сообщений
	bool ChControl        ;// `json:"ChControl"`          контроль входящих ЦХ
	String WorkTime       ;// `json:"WorkTime"`           время работы (круглосуточно, по расписанию)
	bool SSConfirm        ;// `json:"SSConfirm"`          подтверждение тлг категории срочности SS (автоматическое, ручное)
	String QCatSvcLat     ;// `json:"QCatSvcLat"`         категория срочности служебных сообщений лат
	int MaxOutTlgLen      ;// `json:"MaxOutTlgLen"`       максимальная длина сообщения на передачу (символы)
	int MaxInpTlgLen      ;// `json:"MaxInpTlgLen"`       максимальная длина сообщения на прием (символы)
	String ChFormat       ;// `json:"ChFormat"`           (ЦХ, ЦХ ЛР, не передавать)
	int PrevDateLs        ;// `json:"PrevDateLs"`         последний отправленный номер за предыдущие сутки
	int PrevDateLr        ;// `json:"PrevDateLr"`         последний полученный номер за предыдущие сутки
	String Version        ;// `json:"Version"`            версия приложения
	int StatInpBytesCount ;// `json:"StatInpBytesCount"`  кол-во передаваемых конфигуратору Байт принятых телеграм
	int StatOutBytesCount ;// `json:"StatOutBytesCount"`  кол-во передаваемых конфигуратору Байт отправленных телеграм
	String URLAddress     ;// `json:"URLAddress"`         IP адрес для доступа к web страничке
	String URLPath        ;// `json:"URLPath"`            путь для доступа к web страничке
	int URLPort           ;// `json:"URLPort"`            порт для доступа к web страничке
	bool ShowDebugMgs     ;// `json:"ShowDebugMgs"`       показывать отладочные сообщений в журнале

  ChannelSetts(this.role, {this.ID, IsWorking, this.InpNameLat, this.OutNameLat, this.CcmAddressLat, this.OwnAddressLat, this.CcmIpAddress, this.CcmIpPort,
	            this.FdpsWsPort, this.InpNum, this.OutNum, this.OutNumSkipCounter, this.CrLf, this.SplitSignal, this.ChControl, this.WorkTime,
	            this.SSConfirm, this.QCatSvcLat, this.MaxOutTlgLen, this.MaxInpTlgLen, this.ChFormat, this.PrevDateLs, this.PrevDateLr, this.Version,
	            this.StatInpBytesCount , this.StatOutBytesCount, this.URLAddress, this.URLPath, this.URLPort, this.ShowDebugMgs});

}
