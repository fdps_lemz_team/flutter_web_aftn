import 'package:flutter/material.dart';

import 'channel_setts.dart';
//import 'controller_setts.dart';
import 'controller_setts_wgt.dart';
import 'channel_setts_wgt.dart';

List<ChannelSetts> chanSettsList = new List();
//List<ControllerSetts> cntrlSettsList = new List();

TextStyle chanCaptionStyle = new TextStyle(color: Colors.black87, fontSize: 22);
TextStyle chanLabelTextStyle = new TextStyle(color: Colors.black87, fontSize: 18);
TextStyle chanDefTextStyle = new TextStyle(color: Colors.black87, fontSize: 18);
TextStyle chanWarnTextStyle = new TextStyle(color: Colors.yellow, fontSize: 18);
TextStyle chanErrTextStyle = new TextStyle(color: Colors.red, fontSize: 18);



class MainWgt extends StatelessWidget {
  ChannelSettsWgt _chanSettsWgt = new ChannelSettsWgt();
  ControllerSettsWgt _cntrlSettsWgt = new ControllerSettsWgt(ipAddr:'192.168.1.102', chanPort: 30000, imageurl: 'https://flutter.su/favicon.png');

  MainWgt({String ipAddr, int chanPort, String imageurl});

  void addChanSettings(ChannelSetts setts) {
    chanSettsList.add(setts);
  }

  ChannelSettsWgt getChanSettsWgt() {
    return _chanSettsWgt;
  }

  ControllerSettsWgt getCntrlSettsWgt() {
    return _cntrlSettsWgt;
  }

  @override
  Widget build(BuildContext context) {
  
  Object args = ModalRoute.of(context).settings.arguments;
  
  if (args is ChannelSetts) {
    var value = args.role.toString();
    print('get ChannelSetts $value');
    for(int i = 0; i < chanSettsList.length; i++) {
      if (chanSettsList[i].ID == args.ID) {
        chanSettsList[i] = args;
        break;
      }
    }
  } 

   return Scaffold(
     backgroundColor: Colors.grey[200],
     body: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text('Контроллер', style: chanCaptionStyle),
        new Container(
            //color: Colors.black12,
            height: 100.0,
            child: new Table( columnWidths: {300: FlexColumnWidth(1.2)},
            border: TableBorder.all( color :Colors.black87, width : 1.0, style: BorderStyle.solid),
            children: [
              TableRow(
                children: [
                  new Text('IP адрес:', style: chanLabelTextStyle, overflow: TextOverflow.ellipsis),
                  new Text("???", style: chanDefTextStyle, overflow: TextOverflow.ellipsis)
                ]
              ),
              TableRow(
                children: [
                  new Text('Порт AFTN каналов:', style: chanLabelTextStyle, overflow: TextOverflow.ellipsis),
                  new Text("???", style: chanWarnTextStyle, overflow: TextOverflow.ellipsis)
                ]
              ),
              TableRow(
                children: [
                  new Text('Версия ПО:', style: chanLabelTextStyle, overflow: TextOverflow.ellipsis),
                  new Text("???", style: chanErrTextStyle, overflow: TextOverflow.ellipsis)
                ]
              ),
              TableRow(
                children: [
                  RaisedButton.icon(
                    color: Colors.blue,
                      textColor: Colors.white,
                      icon: Icon(Icons.settings),
                      label: Text('Настройки'),
                      onPressed: () { Navigator.pushNamed(context, '/controller_setts');},                      
                    ),              
                    RaisedButton.icon(
                      color: Colors.blue,
                      textColor: Colors.white,
                      icon: Icon(Icons.message),
                      label: Text('Журнал'),
                      onPressed: () {},                      
                    ),
                ]
              )
            ],
            )
        ),   
        Text('', style: chanCaptionStyle),
        Text('Канал', style: chanCaptionStyle),
        new Container(
            //color: Colors.black12,
            height: 100.0,
            child: new Table( columnWidths: {300: FlexColumnWidth(1.2)},
            border: TableBorder.all( color :Color(0xFF000000), width : 1.0, style: BorderStyle.solid),
            children: [
              TableRow(
                children: [
                  new Text('IP адрес:', style: chanLabelTextStyle, overflow: TextOverflow.ellipsis),
                  new Text("???", style: chanDefTextStyle, overflow: TextOverflow.ellipsis)
                ]
              ),
              TableRow(
                children: [
                  new Text('Порт AFTN каналов:', style: chanLabelTextStyle),
                  new Text("???".toString(), style: chanWarnTextStyle, overflow: TextOverflow.ellipsis)
                ]
              ),
              TableRow(
                children: [
                  new Text('Версия ПО:', style: chanLabelTextStyle, overflow: TextOverflow.ellipsis),
                  new Text("???", style: chanErrTextStyle, overflow: TextOverflow.ellipsis)
                ]
              ),
              TableRow(
                children: [
                  RaisedButton.icon(                    
                      color: Colors.blue,
                      textColor: Colors.white,
                      icon: Icon(Icons.settings),
                      label: Text('Настройки'),
                      onPressed: () {
                        Navigator.pushNamed(context, '/channel_setts', arguments: chanSettsList[0]); },
                  ),
                    RaisedButton.icon(
                      color: Colors.blue,
                      textColor: Colors.white,
                      icon: Icon(Icons.message),
                      label: Text('Журнал'),
                      onPressed: () {print(chanSettsList.length.toString());},                      
                    ),
                ]
              )
            ],
            )
        )   
      ],
    )
   );
  }
}