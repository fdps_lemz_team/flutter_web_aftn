import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:validators/validators.dart';

import 'channel_setts.dart';



var _crLfList = <String>['\\r\\n', '\\r\\r\\n']
      .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
        })
      .toList();


class ChannelSettsWgt extends StatefulWidget  {
  ChannelSetts _setts;

  //ChannelSettsWgt({setts});

   ChannelSettsWgt() {
     _setts = ChannelSetts(SettsRole.none);
     _setts = ChannelSetts(SettsRole.none);

  //   _setts.ID = 1;
  //   _setts.InpNameLat = "TZZ";
  //   _setts.OutNameLat = "ZTZ";
   }

  // void setSettings( ChannelSetts setts ) {
  //   _setts = setts;
  // }

@override
  State<StatefulWidget> createState() => ChannelSettsWgtState();
}

class ChannelSettsWgtState extends State<ChannelSettsWgt> {
  final _formKey = GlobalKey<FormState>();
  
  @override
  Widget build(BuildContext context) {
  ChannelSetts args = ModalRoute.of(context).settings.arguments;

  String dropdownValue = 'One';

  if (args != null) {
    widget._setts = args;
  }
  
   return new Scaffold(
     body:Container(padding: EdgeInsets.all(10.0), 
    child: new Form(key: _formKey, 
      //new Column(children: <Widget>[
     //new Scaffold(
     //body:    Container(
        //color: Colors.red[50],
        //height: 100.0,
        child: new Table( columnWidths: {300: FlexColumnWidth(1.2)},
        border: TableBorder.all( color :Color(0xFF000000), width : 1.0, style: BorderStyle.solid),
        children: [
          TableRow(
            children: <Widget>[
             new Text('Идентификатор канала:', style: new TextStyle(fontSize: 20.0), overflow: TextOverflow.ellipsis),
             new TextFormField(initialValue: widget._setts.ID.toString(), readOnly: true ),
            ]
          ),
          TableRow(
            children: <Widget>[
             new Text('Обозначение канала на прием (лат):', style: new TextStyle(fontSize: 20.0), overflow: TextOverflow.ellipsis),
             new TextFormField(initialValue: widget._setts.InpNameLat,
                  validator: (value){ if (value.length != 3) return 'Ведите обозначение канала на прием (лат)'; else widget._setts.InpNameLat = value;},
                  inputFormatters: <TextInputFormatter>[
                    LengthLimitingTextInputFormatter(3),
                    WhitelistingTextInputFormatter(RegExp('[A-Z]')),                    
                  ],
              ),
            ]
          ),
          TableRow(
           children: <Widget>[
            new Text('Обозначение канала на передачу (лат):', style: new TextStyle(fontSize: 20.0), overflow: TextOverflow.ellipsis),
            new TextFormField(initialValue: widget._setts.OutNameLat,
                 validator: (value){ if (value.length != 3) return 'Ведите обозначение канала на передачу (лат)'; else widget._setts.OutNameLat = value;},
                 inputFormatters: <TextInputFormatter>[
                   LengthLimitingTextInputFormatter(3),
                   WhitelistingTextInputFormatter(RegExp('[A-Z]')),                    
                 ],
            ),
           ]
          ),
          TableRow(
            children: <Widget>[
             new Text('Адрес ЦКС (лат):', style: new TextStyle(fontSize: 20.0), overflow: TextOverflow.ellipsis),
             new TextFormField(initialValue: widget._setts.CcmAddressLat,
                  validator: (value){ if (value.isEmpty || value.length != 8) return 'Ведите собственный адрес (лат)'; else widget._setts.CcmAddressLat = value; },
                  inputFormatters: <TextInputFormatter>[
                    LengthLimitingTextInputFormatter(8),
                    WhitelistingTextInputFormatter(RegExp('[A-Z]')),                    
                  ],
             ),
            ]
          ),
          TableRow(
            children: <Widget>[
             new Text('Сообственный адрес (лат):', style: new TextStyle(fontSize: 20.0), overflow: TextOverflow.ellipsis),
             new TextFormField(initialValue: widget._setts.OwnAddressLat,
                  validator: (value){ if (value.isEmpty || value.length != 8) return 'Ведите собственный адрес (лат)'; else widget._setts.OwnAddressLat = value;},
                  inputFormatters: <TextInputFormatter>[
                    LengthLimitingTextInputFormatter(8),
                    WhitelistingTextInputFormatter(RegExp('[A-Z]')),                    
                  ],
             ),
            ]
          ),
          TableRow(
            children: <Widget>[
             new Text('IP адрес ЦКС:', style: new TextStyle(fontSize: 20.0), overflow: TextOverflow.ellipsis),
             new TextFormField(initialValue: widget._setts.CcmIpAddress,
                  validator: (value){ if (!isIP(value)) return 'Ведите IP адрес ЦКС'; else widget._setts.CcmIpAddress = value;},                  
             ),
            ]
          ),
          TableRow(
            children: <Widget>[
             new Text('Сетевой порт ЦКС:', style: new TextStyle(fontSize: 20.0), overflow: TextOverflow.ellipsis),
             new TextFormField(initialValue: widget._setts.CcmIpPort.toString(),
              validator: (value){ if (int.parse(value) < 0 || int.parse(value) > 65535) return 'Ведите сетевой порт ЦКС'; else widget._setts.CcmIpPort = int.parse(value);},
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                LengthLimitingTextInputFormatter(5),
                WhitelistingTextInputFormatter.digitsOnly,
              ],
             ),      
            ]
          ),
          TableRow(
            children: <Widget>[
             new Text('Сетевой порт FDPS:', style: new TextStyle(fontSize: 20.0), overflow: TextOverflow.ellipsis),
             new TextFormField(initialValue: widget._setts.FdpsWsPort.toString(),
                  validator: (value){ if (int.parse(value) < 0 || int.parse(value) > 65535) return 'Ведите сетевой порт FDPS'; else widget._setts.FdpsWsPort = int.parse(value);},
                  keyboardType: TextInputType.number,
                    inputFormatters: <TextInputFormatter>[
                      LengthLimitingTextInputFormatter(5),
                      WhitelistingTextInputFormatter.digitsOnly,
                    ],
             ),
            ]
          ),
          TableRow(
            children: <Widget>[
             new Text('Канальный номер на прием:', style: new TextStyle(fontSize: 20.0), overflow: TextOverflow.ellipsis),
             new TextFormField(initialValue: widget._setts.InpNum.toString(),
                  validator: (value){ if (value.isEmpty) return 'Ведите канальный номер на прием'; else widget._setts.InpNum = int.parse(value); },
                  keyboardType: TextInputType.number,
                    inputFormatters: <TextInputFormatter>[
                      WhitelistingTextInputFormatter.digitsOnly,
                    ],
                  ),
            ]
          ),
          TableRow(
            children: <Widget>[
             new Text('Канальный номер на отправку:', style: new TextStyle(fontSize: 20.0), overflow: TextOverflow.ellipsis),
             new TextFormField(initialValue: widget._setts.OutNum.toString(),
                  validator: (value){ if (value.isEmpty) return 'Ведите канальный номер на отправку'; else widget._setts.OutNum = int.parse(value);},
                  keyboardType: TextInputType.number,
                    inputFormatters: <TextInputFormatter>[
                      WhitelistingTextInputFormatter.digitsOnly,
                    ],
             ),
            ]
          ),
          TableRow(
            children: <Widget>[
             new Text('Кол-во игнорируемых сообщений о подстройке канальных номеров:', style: new TextStyle(fontSize: 20.0), overflow: TextOverflow.ellipsis),
             new TextFormField(initialValue: widget._setts.OutNumSkipCounter.toString(),
                  validator: (value){ if (value.isEmpty) return 'Ведите кол-во игнорируемых сообщений о подстройке канальных номеров'; else widget._setts.OutNumSkipCounter = int.parse(value); },
                  keyboardType: TextInputType.number,
                    inputFormatters: <TextInputFormatter>[
                      WhitelistingTextInputFormatter.digitsOnly,
                    ],
             ),
            ]
          ),
          TableRow(
            children: <Widget>[
             new Text('Символы конца строки :', style: new TextStyle(fontSize: 20.0), overflow: TextOverflow.ellipsis),
             new DropdownButton<String>(
              value: widget._setts.CrLf,
              items: _crLfList,
              onChanged: (String newValue) {
                setState(() {
                  widget._setts.CrLf = newValue;
                  //dropdownValue = newValue;
                });
              },          
             )
            ]
          ),
          TableRow(
            children: [
              RaisedButton.icon(
                color: Colors.blue[200],
                textColor: Colors.white,
                icon: Icon(Icons.check),
                label: Text('Применить'),
                onPressed: () {  
                  if(_formKey.currentState.validate()) {
                    Navigator.pushNamed(context, '/main', arguments: widget._setts);
                  }
                },
              ),
              RaisedButton.icon(
                color: Colors.blue[200],
                textColor: Colors.white,
                icon: Icon(Icons.cancel),
                label: Text('Отмена'),
                onPressed: () {Navigator.pushNamed(context, '/main');},                
              ),              
            ]
          )
        ],
        )
    ) 
     )  
    );
    
   
   

  }
}