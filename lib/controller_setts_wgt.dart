import 'package:flutter/material.dart';

class ControllerSettsWgt extends StatelessWidget {
  String _ipAddr;
  int _chanPort;
  String _imageurl;

  ControllerSettsWgt({String ipAddr, int chanPort, String imageurl}) {
    _ipAddr = ipAddr;
    _chanPort = chanPort;
    _imageurl = imageurl;
  }

  @override
  Widget build(BuildContext context) {
   return new Container(
        color: Colors.red[50],
        height: 100.0,
        child: new Table( columnWidths: {300: FlexColumnWidth(1.2)},
        border: TableBorder.all( color :Color(0xFF000000), width : 1.0, style: BorderStyle.solid),
        children: [
          TableRow(
            children: [
              new Text('1IP адрес:', style: new TextStyle(fontSize: 20.0), overflow: TextOverflow.ellipsis),
              new Text(_ipAddr, style: new TextStyle(fontSize: 20.0), overflow: TextOverflow.ellipsis)
            ]
          ),
           TableRow(
            children: [
              new Text('1Порт AFTN каналов:', style: new TextStyle(fontSize: 20.0), overflow: TextOverflow.ellipsis),
              new Text(_chanPort.toString(), style: new TextStyle(fontSize: 20.0), overflow: TextOverflow.ellipsis)
            ]
          ),
          TableRow(
            children: [
              new Text('1Версия ПО:', style: new TextStyle(fontSize: 20.0), overflow: TextOverflow.ellipsis),
              new Text(_ipAddr, style: new TextStyle(fontSize: 20.0), overflow: TextOverflow.ellipsis)
            ]
          ),
          TableRow(
            children: [
              RaisedButton(
                  onPressed: () {Navigator.pushNamed(context, '/main');},
                  child: const Text('Применить', style: TextStyle(fontSize: 20)),  
                ),              
                RaisedButton(
                  onPressed: () {Navigator.pushNamed(context, '/main');},
                  child: const Text('Отмена', style: TextStyle(fontSize: 20)),  
                ),              
            ]
          )
        ],
        )
    );   
  }
}